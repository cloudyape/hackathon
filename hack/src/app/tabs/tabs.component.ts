import { Component, OnInit , HostListener } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {
  currentSelection:any;
  openBuildingsVar:any;
  floor:any = [];
  bay:any = [];
  openBayVar:boolean = false;
  openBayDetails:any;
  bayNo:any;
  selectedBays:any;
  selectedFloors:any;
  selectedBuildingIndex:any;
  selectedSites:any;
  selectedLocation:any; 
  details:any = [];
  bayTower:any;
  siteshort:any;
  selectedIndex:any;
  bayi:any;
  bayj:any;
  location:any = [ 
    { "location" : "Bengaluru"} , 
    { "location" : "Chennai"} , 
    { "location" : "Gurugram"} , 
    { "location" : "Hyderabad"} , 
    { "location" : "Mumbai"} , 
  ];

  sites:any = [
    { "sitename" : "BDC 1" , "location" : "Bengaluru"} , 
    { "sitename" : "BDC 2" , "location" : "Bengaluru"} , 
    { "sitename" : "BDC 3" , "location" : "Bengaluru"} ,
    { "sitename" : "BDC 4" , "location" : "Bengaluru"} , 
    { "sitename" : "BDC 5" , "location" : "Bengaluru"} , 
    { "sitename" : "BDC 6" , "location" : "Bengaluru"} , 
    { "sitename" : "BDC 7" , "location" : "Bengaluru"} , 
    { "sitename" : "BDC 8" , "location" : "Bengaluru"} , 
    { "sitename" : "BDC 9" , "location" : "Bengaluru"} , 
    { "sitename" : "BDC 10" , "location" : "Bengaluru"} , 
    { "sitename" : "BDC 11" , "location" : "Bengaluru"} ,
    { "sitename" : "DDC 1" , "location" : "Gurugram"} , 
    { "sitename" : "DDC 2" , "location" : "Gurugram"} , 
    { "sitename" : "DDC 3" , "location" : "Gurugram"} , 
    { "sitename" : "DDC 4" , "location" : "Gurugram"} , 
    { "sitename" : "DDC 5" , "location" : "Gurugram"} , 
    { "sitename" : "CDC 1" , "location" : "Chennai"} , 
    { "sitename" : "CDC 2" , "location" : "Chennai"} , 
    { "sitename" : "CDC 3" , "location" : "Chennai"} , 
    { "sitename" : "CDC 4" , "location" : "Chennai"} , 
    { "sitename" : "CDC 5" , "location" : "Chennai"} , 
    { "sitename" : "HDC 1" , "location" : "Hyderabad"} , 
    { "sitename" : "HDC 2" , "location" : "Hyderabad"} , 
    { "sitename" : "HDC 3" , "location" : "Hyderabad"} , 
    { "sitename" : "HDC 4" , "location" : "Hyderabad"} , 
    { "sitename" : "HDC 5" , "location" : "Hyderabad"} ,
    { "sitename" : "HDC 6" , "location" : "Hyderabad"} , 
    { "sitename" : "MDC 1" , "location" : "Mumbai"} ,
    { "sitename" : "MDC 2" , "location" : "Mumbai"} , 
  ]
  
  building:any = [
    { "sitename" : "BDC 1" , "tower" : "Tower A"} , 
    { "sitename" : "BDC 1" , "tower" : "Tower B"} , 
    { "sitename" : "BDC 1" , "tower" : "Tower C"} , 
    { "sitename" : "BDC 2" , "tower" : "Tower A"} ,
    { "sitename" : "BDC 2" , "tower" : "Tower B"} , 
    { "sitename" : "BDC 3" , "tower" : "Tower A"} , 
    { "sitename" : "BDC 4" , "tower" : "Tower B"} , 
    { "sitename" : "BDC 4" , "tower" : "Tower C"} , 
    { "sitename" : "BDC 5" , "tower" : "Tower A"} ,
    { "sitename" : "BDC 5" , "tower" : "Tower B"} , 
    { "sitename" : "BDC 6" , "tower" : "Tower A"} ,
    { "sitename" : "BDC 6" , "tower" : "Tower B"} , 
    { "sitename" : "BDC 7" , "tower" : "Tower A"} ,
    { "sitename" : "BDC 8" , "tower" : "Tower B"} , 
    { "sitename" : "BDC 9" , "tower" : "Tower A"} ,
    { "sitename" : "BDC 9" , "tower" : "Tower B"} , 
    { "sitename" : "BDC 10" , "tower" : "Tower A"} ,
    { "sitename" : "BDC 10" , "tower" : "Tower B"} , 
    { "sitename" : "BDC 11" , "tower" : "Tower A"} ,
    { "sitename" : "DDC 1" , "tower" : "Tower A"} , 
    { "sitename" : "DDC 2" , "tower" : "Tower B"} ,
    { "sitename" : "DDC 3" , "tower" : "Tower A"} , 
    { "sitename" : "DDC 3" , "tower" : "Tower B"} ,
    { "sitename" : "DDC 4" , "tower" : "Tower A"} , 
    { "sitename" : "DDC 5" , "tower" : "Tower A"} , 
    { "sitename" : "DDC 5" , "tower" : "Tower B"} , 
    { "sitename" : "DDC 5" , "tower" : "Tower C"} , 
    { "sitename" : "HDC 1" , "tower" : "Tower A"} , 
    { "sitename" : "HDC 2" , "tower" : "Tower B"} ,
    { "sitename" : "HDC 3" , "tower" : "Tower A"} , 
    { "sitename" : "HDC 3" , "tower" : "Tower B"} ,
    { "sitename" : "HDC 4" , "tower" : "Tower A"} , 
    { "sitename" : "HDC 5" , "tower" : "Tower A"} , 
    { "sitename" : "HDC 5" , "tower" : "Tower B"} , 
    { "sitename" : "HDC 5" , "tower" : "Tower C"} , 
    { "sitename" : "HDC 6" , "tower" : "Tower A"} , 
    { "sitename" : "CDC 1" , "tower" : "Tower A"} , 
    { "sitename" : "CDC 2" , "tower" : "Tower A"} ,
    { "sitename" : "CDC 2" , "tower" : "Tower B"} ,
    { "sitename" : "CDC 2" , "tower" : "Tower C"} ,
    { "sitename" : "CDC 2" , "tower" : "Tower D"} ,
    { "sitename" : "CDC 3" , "tower" : "Tower A"} , 
    { "sitename" : "CDC 3" , "tower" : "Tower B"} ,
    { "sitename" : "CDC 4" , "tower" : "Tower A"} , 
    { "sitename" : "CDC 5" , "tower" : "Tower A"} , 
    { "sitename" : "CDC 5" , "tower" : "Tower B"} , 
    { "sitename" : "CDC 5" , "tower" : "Tower C"} , 
    { "sitename" : "MDC 1" , "tower" : "Tower A"} , 
    { "sitename" : "MDC 1" , "tower" : "Tower B"} , 
    { "sitename" : "MDC 1" , "tower" : "Tower C"} , 
    { "sitename" : "MDC 2" , "tower" : "Tower A"} ,
    { "sitename" : "MDC 2" , "tower" : "Tower B"} ,
  ]
  

  openSitesVar:any;
  openFloorVar:any;
  indexRow = -1;
  functionSelect:any = 0;
  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) { 
    //this.key = event.key;
    if(event.keyCode == 13) {
      this.functionSelect = this.functionSelect + 1;
      if(this.indexRow == 0) {
        this.siteshort = 0;
        this.selectedSitesFn(this.siteshort);
        this.openBuildings(this.sites[this.siteshort].sitename);
      }
      if(this.indexRow == 1) {
        this.siteshort = 16;
        this.selectedSitesFn(this.siteshort);
        this.openBuildings(this.sites[this.siteshort].sitename);
      }
      if(this.indexRow == 2) {
        this.siteshort = 11;
        this.selectedSitesFn(this.siteshort);
        this.openBuildings(this.sites[this.siteshort].sitename);
      }
      if(this.indexRow == 3) {
        this.siteshort = 21;
        this.selectedSitesFn(this.siteshort);
        this.openBuildings(this.sites[this.siteshort].sitename);
      }
      if(this.indexRow == 4) {
        this.siteshort = 27;
        this.selectedSitesFn(this.siteshort);
        this.openBuildings(this.sites[this.siteshort].sitename);
      }
    }
    if(event.keyCode == 40) {
      if(this.functionSelect == 0) {
        if(this.indexRow < 4) {
          this.indexRow = this.indexRow + 1;
          this.setRow(this.indexRow);
          this.openSites(this.location[this.indexRow].location);
        }
      }

      if(this.functionSelect == 1) {
        if(this.indexRow == 0) {
          if(this.siteshort < 10 && this.siteshort > -1 ) {
            this.siteshort = this.siteshort + 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
        if(this.indexRow == 1) {
          
          if(this.siteshort < 20 && this.siteshort > 14 ) {
            this.siteshort = this.siteshort + 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
        if(this.indexRow == 2) {
          
          if(this.siteshort < 15 && this.siteshort > 10 ) {
            this.siteshort = this.siteshort + 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
        if(this.indexRow == 3) {
          
          if(this.siteshort < 26 && this.siteshort > 20 ) {
            this.siteshort = this.siteshort + 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
        if(this.indexRow == 4) {
          
          if(this.siteshort < 28 && this.siteshort > 26 ) {
            this.siteshort = this.siteshort + 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
      }
    }

    if(event.keyCode == 38) {
      if(this.functionSelect == 0) {
        if(this.indexRow > 0) {
          this.indexRow = this.indexRow - 1;
          this.setRow(this.indexRow);
          this.openSites(this.location[this.indexRow].location);
        }
      }
      if(this.functionSelect == 1) {
        if(this.indexRow == 0) {
          if(this.siteshort < 11 && this.siteshort > 0 ) {
            this.siteshort = this.siteshort - 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
        if(this.indexRow == 1) {
          
          if(this.siteshort < 21 && this.siteshort > 14 ) {
            this.siteshort = this.siteshort - 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
        if(this.indexRow == 2) {
          
          if(this.siteshort < 16 && this.siteshort > 10 ) {
            this.siteshort = this.siteshort - 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
        if(this.indexRow == 3) {
          
          if(this.siteshort < 27 && this.siteshort > 20 ) {
            this.siteshort = this.siteshort - 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
        if(this.indexRow == 4) {
          
          if(this.siteshort < 29 && this.siteshort > 26 ) {
            this.siteshort = this.siteshort - 1;
            this.selectedSitesFn(this.siteshort);
            this.openBuildings(this.sites[this.siteshort].sitename);
          }
        }
        this.selectedSitesFn(this.selectedSites);
      }
    }
  }

  openSites(getlocation:any) {
    this.openBuildings(" ");
    this.openFloor(" " , " ");
    this.details = [];
    this.floor = [];
    this.openBayVar = false;
    this.openSitesVar = getlocation;
    console.log(this.openSitesVar);
  }

  openBuildings(getlocation:any) {
    this.floor = [];
    this.openFloor(" " , " ");
    this.openBayVar = false;
    this.details = [];
   
    this.openBuildingsVar = getlocation;
    console.log(this.openBuildingsVar);
  }

  openFloor(getLocation:any , getTower:any) {
    this.openBayVar = false;
    this.floor = [];
    this.details = [];
    this.openFloorVar = getTower;
    if(getLocation.indexOf("MDC") > -1) {
      this.floor = [];
      let i;
      for(i = 1; i < 3; i++) {
        if(getLocation == "MDC " + 1) {
          this.floor = [];
          if(getTower == 'Tower A') {
            this.floorDetails(getTower , getLocation , 10);
          }
          if(getTower == 'Tower B') {
            this.floorDetails(getTower , getLocation , 7);
          }
          if(getTower == 'Tower C') {
           this.floorDetails(getTower , getLocation , 11);
          }
          console.log(this.floor);
        }
        if(getLocation == "MDC " + 2) {
          this.floor = [];
          if(getTower == 'Tower A') {
            this.floorDetails(getTower , getLocation , 5);
          }
          if(getTower == 'Tower B') {
            this.floorDetails(getTower , getLocation , 8);
          }
          console.log(this.floor);
        }
      }
    }

    if(getLocation.indexOf("HDC") > -1) {
      this.floor = [];
      for(let i = 1; i < 7; i++) {
        this.floor = [];
          if(getTower == 'Tower A') {
            this.floorDetails(getTower , getLocation , 10);
          }
          if(getTower == 'Tower B') {
            this.floorDetails(getTower , getLocation , 6);
          }
          console.log(this.floor);

          if(getTower == 'Tower C') {
            this.floorDetails(getTower , getLocation , 11);
          }

          if(getLocation == "HDC " + 2) {
            this.floor = [];
            if(getTower == 'Tower B') {
              this.floorDetails(getTower , getLocation , 3);
            }
          }
          console.log(this.floor);
      }
    }

    if(getLocation.indexOf("DDC") > -1) {
      this.floor = [];
      for(let i = 1; i < 7; i++) {
        this.floor = [];
          if(getTower == 'Tower A') {
            this.floorDetails(getTower , getLocation , 10);
          }
          if(getTower == 'Tower B') {
            this.floorDetails(getTower , getLocation , 6);
          }
          console.log(this.floor);

          if(getTower == 'Tower C') {
            this.floorDetails(getTower , getLocation , 11);
          }

          if(getLocation == "DDC " + 2) {
            this.floor = [];
            if(getTower == 'Tower B') {
              this.floorDetails(getTower , getLocation , 6);
            }
          }
          console.log(this.floor);
      }
    } 

    if(getLocation.indexOf("CDC") > -1) {
      this.floor = [];
      for(let i = 1; i < 7; i++) {
        this.floor = [];
          if(getTower == 'Tower A') {
            this.floorDetails(getTower , getLocation , 10);
          }
          if(getTower == 'Tower B') {
            this.floorDetails(getTower , getLocation , 6);
          }
          console.log(this.floor);

          if(getTower == 'Tower C') {
            this.floorDetails(getTower , getLocation , 11);
          }

          if(getLocation == "CDC " + 2) {
            this.floor = [];
            if(getTower == 'Tower A') {
              this.floorDetails(getTower , getLocation , 2);
            }
            if(getTower == 'Tower D') {
              this.floorDetails(getTower , getLocation , 11);
            }
            if(getTower == 'Tower B') {
              this.floorDetails(getTower , getLocation , 10);
            }
            if(getTower == 'Tower C') {
              this.floorDetails(getTower , getLocation , 9);
            }
          }
          console.log(this.floor);
      }
    }

    if(getLocation.indexOf("BDC") > -1) {
      this.floor = [];
      for(let i = 1; i < 7; i++) {
        this.floor = [];
          if(getTower == 'Tower A') {
            this.floorDetails(getTower , getLocation , 10);
          }
          if(getTower == 'Tower B') {
            this.floorDetails(getTower , getLocation , 10);
          }
          console.log(this.floor);

          if(getTower == 'Tower C') {
            this.floorDetails(getTower , getLocation , 10);
          }

          if(getLocation == "BDC " + 11) {
            this.floor = [];
            if(getTower == 'Tower A') {
              this.floorDetails(getTower , getLocation , 11);
            }
          }
          console.log(this.floor);
      }
    }
  }

  openBay(getTower:any, i:any , j:any) {
    this.openBayVar = true;
    this.bayNo = getTower;
    this.bay = [];
    if( i == "MDC 1" && j == "Floor1" || i == "MDC 1" && j == "Floor3" || i == "MDC 2") {
      if(this.bayNo == "Tower A") {
      for(let k = 1 ; k < 5 ; k++) {
        this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
      //this.details.push({"seats" : 94 , "serials" : j + "01" , "serialE" : j + "94" , "project" : "Travelers" , "access" : "Open Bay" , "captions" : "03" });
      }
      
    }
      console.log(this.details);
      console.log(this.bay);
      if(this.bayNo == "Tower B") {
        for(let k = 1 ; k < 2 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower C") {
        for(let k = 1 ; k < 5 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
  }

   else if( i == "DDC 5" && j == "Floor1" || i == "DDC 2" && j == "Floor1" || i == "DDC 2") {
      if(this.bayNo == "Tower B") {
        for(let k = 1 ; k < 2 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower A") {
        for(let k = 1 ; k < 2 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower C") {
        for(let k = 1 ; k < 5 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
    }

   else if( i == "HDC 6" && j == "Floor1" || i == "HDC 3" && j == "Floor1" || i == "HDC 2") {
      if(this.bayNo == "Tower B") {
        for(let k = 1 ; k < 7 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower A") {
        for(let k = 1 ; k < 6 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower C") {
        for(let k = 1 ; k < 4 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower D") {
        for(let k = 1 ; k < 5 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      
    }

  else if( i == "BDC 6" && j == "Floor1" || i == "BDC 7" && j == "Floor4" || i == "BDC 11") {
      if(this.bayNo == "Tower A") {
        for(let k = 1 ; k < 3 ; k++) {
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower B") {
        for(let k = 1 ; k < 2 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower C") {
        for(let k = 1 ; k < 5 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
    }
    else {
      if(this.bayNo == "Tower A") {
        for(let k = 1 ; k < 4 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower B") {
        for(let k = 1 ; k < 2 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower C") {
        for(let k = 1 ; k < 5 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        console.log(this.bay);
      }
      if(this.bayNo == "Tower D") {
        for(let k = 1 ; k < 3 ; k++) {
          this.bay.push ( {"floors" : "Floor" + j , "bays" : "Bay" + k });
        }
        
        console.log(this.bay);
      }
    }
  }

  floorDetails(getTower:any , i:any,jshow:any) {
    this.floor = [];
    for(let j = 1; j < jshow; j++) {
      this.floor.push ( {"sitename" : i , "floors" : "Floor" + j });
      this.bayTower = getTower;
      this.bayi = i;
      this.bayj = j;
    }
  }

  openDetails(getBay:any) {
    this.details = [];
   this.openBayDetails = getBay;
   console.log(getBay);
   if(this.openBayDetails == 'Bay1') {
   this.details = [{"seats" : 94 , "serials" : getBay + "01" , "serialE" : getBay + "94" , "project" : "XD Studio" , "access" : "Open Bay" , "captions" : "03" }];
   console.log(this.details);
  }
  else if(this.openBayDetails == 'Bay2') {
   this.details = [{"seats" : 94 , "serials" : getBay + "01" , "serialE" : getBay + "94" , "project" : "Travelers" , "access" : "Open Bay" , "captions" : "03" }];
   console.log(this.details);
  }

  else if(this.openBayDetails == 'Bay3') {
    this.details = [{"seats" : 94 , "serials" : getBay + "01" , "serialE" : getBay + "94" , "project" : "BHP" , "access" : "Open Bay" , "captions" : "03" }];
    console.log(this.details);
   }
   else if(this.openBayDetails == 'Bay4') {
    this.details = [{"seats" : 94 , "serials" : getBay + "01" , "serialE" : getBay + "94" , "project" : "Testra" , "access" : "Open Bay" , "captions" : "03" }];
    console.log(this.details);
   }
   else if(this.openBayDetails == 'Bay5') {
    this.details = [{"seats" : 94 , "serials" : getBay + "01" , "serialE" : getBay + "94" , "project" : "Microsoft" , "access" : "Open Bay" , "captions" : "03" }];
    console.log(this.details);
   } else {
    this.details = [{"seats" : 94 , "serials" : getBay + "01" , "serialE" : getBay + "94" , "project" : "Accenture Internal" , "access" : "Open Bay" , "captions" : "03" }];
    console.log(this.details);
   }
  
}

setRow(_index: number) {
  this.selectedIndex = _index;
  this.selectedLocation = _index;
  this.selectedFloors = -1;
  this.selectedBays = -1;
  this.selectedBuildingIndex = -1;
  this.selectedSites = -1;
}

selectedBaysFn(_index: number) {
this.selectedBays = _index;
}

selectedFloorsFn(_index: number) {
  this.selectedFloors = _index;
  this.selectedBays = -1;
}
selectedBuildingIndexFn(_index: number) {
  console.log(_index);
  this.selectedFloors = -1;
  this.selectedBays = -1;
  this.selectedBuildingIndex = _index;
}
selectedSitesFn(_index: number) {
  this.selectedSites = _index;
  this.selectedFloors = -1;
  this.selectedBays = -1;
  this.selectedBuildingIndex = -1;
}
  constructor() { }

  ngOnInit() {
  }



}
